
// weights are based on specificity and common usage
// if a tag is not included here, the tool will resort to calculating by post count (not a good idea)

tag_weights = {
"invalid_tag":					-3.1,
"tagme":						-4.6,
"<uncommon>":					-2.1, // could be obsoleted by estimating weight

"<artist>":						2.7,
"conditional_dnp":				0,
"unknown_artist":				-1.0,
"anonymous_artist":				2.4,
"<copyright>":					2.2,
"<character>":					1.8,
"<species>":					0.7, //extra when tag is not present here
"<meta>":						1.6, //
"<lore>":						1.1,
//"<franchise>":					2.9,
//"<company>":					1.2,

"<year>":						2.1,

// no score since added automatically
"hi_res":						0,
"absurd_res":					0,
"superabsurd_res":				0,
"low_res":						0,


"butt":							0.7,
"breasts":						0.7,
"anus":							1.2,
"pussy":						1.2,
"penis":						1.2,
"nipples":						1.5,
"knot":							1.5,
"balls":						1.5,
"areola":						1.8,
"genitals":						0.2,
"anthro":						0.4,
"feral":						0.4,
"biped":						2.5,
"mammal":						0.2,

"female":						0.9,
"male":							0.9,
"ambiguous_gender": 			1.1,
"gynomorph":					1.1,
"intersex":	    				1.1,

"solo":							1.4,
"duo":							0.4,
"group":						1.2,
"fur":							0.7,
"hair":							0.7,
"clothing":						2.2,
"nude":							2.2,
"clothed":						0.6,
"partially_clothed":			0.9,

"simple_background":			0.2,

"white_background": 			0.8,

"detailed_background":			0.2,

"not_furry":					4.3,
"text":							0.6,
"english_text":					3.3,
"slightly_chubby":				2.5,
"overweight":					1.1,
"obese":					    1.6,

"bodily_fluids":				0.3,
"gential_fluids":				0.5,
"cum":							1.4,
"pussy_juice":					2.7,
"saliva":						2.9,

"canid":						0.1,
"canine":						0.2,
"domestic_dog":					0.3,

"felid":						0.1,
"felis":						0.2,
"feline":						0.2,
"domestic_cat":					0.3,

"equid":						0.1,
"equine":						0.2,
"horse":						0.2,
"pony":							0.4,
"unicorn":						0.4,
"pegasus":						0.5,

"wolf":							0.5,
"fox":							0.5,

"ursid":						2.2,

"erection":						2.7,
"video_games":					0.1,
"sex":							1.1,
"smile":						1.4,

"blush":						2.3,
"sweat":						2.3,

"digital_media_(artwork)":		2.3,
"traditional_media_(artwork)":	2.3,
"digital_drawing_(artwork)":		2.5,
"traditional_drawing_(artwork)":	2.5,

"mixed_media":					1.9,

"monochrome":					2.2,

"open_mouth":					0.7,
"open_smile":					1.4,

"looking_at_viewer":			2.0,
"looking_back":					2.0,

"penetration":					0.5,

"male/female":					2.1,
"male/male":					2.1,

"male_penetrating":				2.6,
"female_penetrating":			2.6,

"male_penetrated":				0.6,
"female_penetrated":			0.6,

"male_penetrating_male":		2.8,
"male_penetrating_female":		2.8,
"female_penetrating_male":		2.8,
"female_penetrating_female":	2.8,

"vaginal":						0.4,
"anal":							0.4,
"oral":							0.4,

"anal_penetration":				1.1,
"vaginal_penetration":			1.1,
"oral_penetration":				1.1,

"belly":						1.1,
"big_belly":					1.5,
"navel":						2.7,

"tongue":						0.9,
"tongue_out":					2.2,

"flat_chested":					0.4,
"small_breasts":				0.4,
"medium_breasts":				0.4,
"big_breasts":					0.3,
"huge_breasts":					0.1,
"hyper_breasts":				0.1,

"big_butt":						0.1,
"huge_butt":					0.2,
"hyper_butt":					0.2,

"big_penis":					0.1,
"huge_penis":					0.2,
"hyper_penis":					0.2,

"big_balls":					0.2,
"huge_balls":					0.3,
"hyper_balls":					0.3,

"macro":						1.8,
"micro":						1.8,

"horn":							2.4,

//"nintendo"

"wings":						2.4,

"humanoid_genitalia":			2.7,
"humanoid_penis":				1.1,


"pok\u00e9mon":					1.3,
"pokemon":						1.3,
"pokeball":						2.3,
"pokéball":						2.3,
"pokémon_(species)":			0.5,
"pokemon_(species)":			0.5,

"animal_genetalia":				2.8,
"animal_penis":					1.1,

"teeth":						2.1,
"claws":						2,
"fangs":						2.3,


"standing":						1.5,
"lying":						1.7,
"on_side":						1.8,
"on_back":						1.9,
"sitting":						1.6,
"kneeling":						1.9,
"all_fours":					1.9,

"muscular":						2.8,
"abs":							2.9,
"pecs":							2.9,
"biceps":						3,

"humanoid":						1.1,
"human":						1.7,

"topwear":						0.4,
"bottomwear":					0.4,

"topless":						1.6,
"bottomless":					1.6,
"eyewear":						0.5,
"legwear":						0.5,
"handwear":						0.5,
"footwear":						0.5,

"swimwear":						1.8,

"cum_inside":					1.3,
"cum_in_pussy":					0.6,
"cum_in_anus":					0.6,
"cum_in_mouth":					0.6,

"penile":						2.2,

"spreading":					2.7,

"eyes_closed":					2.4,
"narrowed_eyes":				2.2,
"half-closed_eyes":				0.4,
"blind":						1.8,

"fingers":						2.3,
"toes":							2.4,
"paws":							2.5,
"feet":							2.2,
"pawpads":						0.6,

"dragon":						2.3,

"bra":							2.6,
"underwear":					2.4,
"panties":						1.3,

"short_hair":					1.1,
"long_hair":					1.1,

"outside":						2.1,
"inside":						2.1,

"comic":						2.4,

"piercing":						2.5,
"ear_piercing":					1.3,

"furniture":					2.1,

"interspecies":					2.8,
"intersex":						2.3,

"spread_legs":					0.5,

"dialogue":						2.2,

"tuft":							2.3,
"chest_tuft":					1.3,
"inner_ear_fluff":				1.3,

"feathers":						2.8,

"<3":							2.9,

"size_difference":				3.1,

"young":						0.4,
"cub":							2.1,
"shota":						2.1,
"loli":							2.1,

"orgasm":						2.5,
"orgasm_face":					0.7,
"looking_pleasured":			1.9,

"lagomorph":					2.3,
"leporid":						0.4,
"rabbit":						0.2,

"solo_focus":					3,

"thick_thighs":					1.7,

"headgear":						2.2,
"headwear":						2.2,

"shirt":						2.3,
"sweater":						2.4,

"pants":						2.3,

"feathered_wings":				0.9,

"scalie":						2.3,
"reptile":						0.5,

"avian":						2.1,
"bird":							1.3,
"cetacean":						2.1,
"marsupial":					2.1,

"primate":						1.1,

"rodent":						1.6,

"collar":						2,

"multicolored_body":			2.4,
"multicolored_fur":				2.4,
"multicolored_hair":			2.4,

"two_tone_body":				1.3,
"two_tone_fur":					1.3,
"two_tone_hair":				1.3,

"presenting":					2.1,
"presenting_anus":				0.4,
"presenting_breasts":			0.4,
"presenting_cloaca":			0.5, // still not sure exactly what this means yet
"presenting_hindquarters":		0.3,
"presenting_penis":				0.3,
"presenting_balls":				0.4,
"presenting_pussy":				0.4,
"presenting_crotch":			0.4,
"presenting_belly":				0.6,
"presenting_sheath":			0.6,

"#_fingers":					2.5,
"#_toes":						2.8,

"pantherine":					0.5,

"wide_hips":					2.1,
"small_waist":					2.3,

"glasses":						1.6,

"anthrofied":					1.8,

"jewelry":						2.2,
"necklace":						2.5,

"bed":							3.1,

"eyelashes":					2.8,
"eyeshadow":					3.5,

"vein":							3.8,

"cutie_mark":					4.2,

"ejaculation":					3.3,
"masturbation":					3.4,

"hat":							3.1,

"markings":						3.2,

"non-mammal_breasts":			3.3,

"barefoot":						3.6,

"fellatio":						2.4,

"bound":						3.1,

"signature":					2.7,
"watermark":					2.7,

"bovid":						2.1,

"licking":						2.2,

"from_behind_position":			2.4,

"anthro_on_anthro":				2.6,

"gloves":						1.3,

"hybrid":						2.2,

"fan_character":				1.5,

"plant":						1.7,

"pose":							2.3,

"equine_penis":					2.9,

"food":							2,

"precum":						2.8,

"sketch":						3,

"uncut":						2.9,

"backsack":						3.2,

"bdsm":							2.7,
"bondage":						2.8,

"toe_claws":					3.1,

"stripes":						3.4,

"sex_toy":						3.2,

"clitoris":						1.9,

"bulge":						2.1,

"animated":						1.7,

"membranous_wings":				2.4,

"marine":						2.1,

"raised_tail":					2.7,

"one_eye_closed":				2.8,
"wink":							1.3,

"hooves":						3.1,

"sky":							3.6,

"canine_penis":					2.9,

"hyper":						2.1,

"weapon":						3.2,

"rear_view":					3.5,
"front_view":					3.5,
"side_view":					3.5,

"group_sex":					3.7,

"eyebrows":						3.8,

"water":						3.4,

"earth_pony":					3.6,

"portrait":						3.2,

"tree":							3.5,

"girly":						3.5,

"cumshot":						3.1,

"submissive":					3.8,
"dominant":						3.8,

"holding_object":				3.7,
"cleavage":						3.9,

"scales":						3,

"stockings":					3.4,

"3d_(artwork)":					2.9,

//"speech_bubble":				0, //why idr

"curvy_figure":					2.5,

"tears":						2.8,

"<COL>_body":					2.2,

"<COL>_fur":					2.2,

"<COL>_eyes":					2.3,

"<COL>_nose":					2.4,

"<COL>_tail":					2.5,

"<COL>_background":				2.0,

"<COL>_hair":				2.3,

}
